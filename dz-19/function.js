// Площадь прямоугольника:

// Function Declaration
function area(a,b) {
    let s = a * b
    return s
}
console.log('Площадь квадрата : ' + area(3,5) )

// Function Expression:
let area = function area(a,b) {
    let s = a * b
    return s
}
console.log('Площадь квадрата : ' + area(3,5) )

// arrow functions:
let area = (a,b) => a * b
console.log('Площадь квадрата : ' + area(3,5))

//Теорема Пифагора :

// Function Declaration
function pythagoreanTheorem(a,b,c) {
    let sqrt_a = a * a
    let sqrt_b = b * b
    let sqrt_c = c * c
    if (sqrt_a + sqrt_b === sqrt_c) {
        return 'Это прямоугольный треугольник !'
    } else if(sqrt_a + sqrt_b > sqrt_c) {
        return 'Это остроугольный треугольник !'
    } else if (sqrt_c > sqrt_a + sqrt_b) {
        return 'Это тупоугольный треугольник !'
    } else {
        'Не подходит под теорему'
    }
}
console.log(pythagoreanTheorem(3,2,1))

// Function Expression:
let pythagoreanTheorem = function pythagoreanTheorem(a,b,c) {
    let sqrt_a = a * a
    let sqrt_b = b * b
    let sqrt_c = c * c
    if (sqrt_a + sqrt_b === sqrt_c) {
        return 'Это прямоугольный треугольник !'
    } else if (sqrt_a + sqrt_b > sqrt_c) {
        return 'Это остроугольный треугольник !'
    } else if (sqrt_c > sqrt_a + sqrt_b) {
        return 'Это тупоугольный треугольник !'
    } else {
        'Не подходит под теорему'
    }
}
console.log(pythagoreanTheorem(3,3,5))

// arrow functions:
let pythagoreanTheorem = (a,b,c) => {
    let sqrt_a = a * a
    let sqrt_b = b * b
    let sqrt_c = c * c
    if (sqrt_a + sqrt_b === sqrt_c) {
        return 'Это прямоугольный треугольник !'
    } else if (sqrt_a + sqrt_b > sqrt_c) {
        return 'Это остроугольный треугольник !'
    } else if (sqrt_c > sqrt_a + sqrt_b) {
        return 'Это тупоугольный треугольник !'
    } else {
        'Не подходит под теорему'
    }
}
console.log(pythagoreanTheorem(3,3,5))

//Дискриминант:

// Function Declaration:
function discriminant(a,b,c) {
    let discr = (b * b) - 4*(a * c)
    let sqrtDiscr = Math.sqrt(discr)
    if (discr < 0) {
        return 'D < 0 - корней нет'
    }
    if (discr === 0) {
        let x = (-b + sqrtDiscr) / 2 * a
        return `D = 0, значит уравнение имеет один корень = ${x}`
    }
    if (discr > 0) {
        let x_1 = (-b + sqrtDiscr) / 2 * a
        let x_2 = (-b - sqrtDiscr) / 2 * a
        return `D > 0, значит два корня = ${x_1}, ${x_2}`
    }
}
console.log(discriminant(1,12,36))

// Function Expression:
let discriminant = function discriminant(a,b,c) {
    let discr = (b * b) - 4*(a * c)
    let sqrtDiscr = Math.sqrt(discr)
    if (discr < 0) {
        return 'D < 0 - корней нет'
    }
    if (discr === 0) {
        let x = (-b + sqrtDiscr) / 2 * a
        return `D = 0, значит уравнение имеет один корень = ${x}`
    }
    if (discr > 0) {
        let x_1 = (-b + sqrtDiscr) / 2 * a
        let x_2 = (-b - sqrtDiscr) / 2 * a
        return `D > 0, значит два корня = ${x_1}, ${x_2}`
    }
}
console.log(discriminant(1,12,36))

// arrow functions:
let discriminant = (a,b,c) => {
    let discr = (b * b) - 4*(a * c)
    let sqrtDiscr = Math.sqrt(discr)
    if (discr < 0) {
        return 'D < 0 - корней нет'
    }
    if (discr === 0) {
        let x = (-b + sqrtDiscr) / 2 * a
        return `D = 0, значит уравнение имеет один корень = ${x}`
    }
    if (discr > 0) {
        let x_1 = (-b + sqrtDiscr) / 2 * a
        let x_2 = (-b - sqrtDiscr) / 2 * a
        return `D > 0, значит два корня = ${x_1}, ${x_2}`
    }
}
console.log(discriminant(1,12,36))

// Четные числа до 100:

// Function Declaration:
function evenNumbers (num) {
   for (let i = 2; i < num; i+=2) {
       console.log(i)
   }
}
evenNumbers(100)

// Function Expression:
let evenNumbers = function evenNumbers (num) {
    for (let i = 2; i < num; i+=2) {
        console.log(i)
    }
}
evenNumbers(100)

// arrow functions:
let evenNumbers = (num) => {
        for (let i = 2; i < num; i+=2) {
                console.log(i)
            }
    }
evenNumbers(100)

// Нечетные числа до 100:

// Function Declaration:
function evenNumbers (num) {
   for (let i = 1; i < num; i+=2) {
       console.log(i)
   }
}
evenNumbers(100)

// Function Expression:
let evenNumbers = function evenNumbers (num) {
    for (let i = 1; i < num; i+=2) {
        console.log(i)
    }
}
evenNumbers(100)

// arrow functions:
let evenNumbers = (num) => {
        for (let i = 2; i < num; i+=2) {
                console.log(i)
            }
    }
evenNumbers(100)

// Нахождение числа в степени:

// Function Declaration:
function pow (num, n) {
    let b = num
    for (let i = 1; i < n; i++) {
        b *= num
    }
    return b
}
console.log(pow(4, 6))

// Function Expression:
let pow = function pow (num, n) {
    let b = num
    for (let i = 1; i < n; i++) {
        b *= num
    }
    return b
}
console.log(pow(4, 6))

// Function arrow:
let pow = (num, n) => {
    let b = num
    for (let i = 1; i < n; i++) {
        b *= num
    }
    return b
}
console.log(pow(4, 6))

// Функция сортировки

// Function Declaration:
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function sort(arr, ascDesc) {
    let len_arr = arr.length
    if (ascDesc === 'asc' || !ascDesc) {
        for (let j = len_arr - 1; j > 0; j--) {
            for (let i = 0; i < j; i++) {
                if (arr[i] > arr[i + 1]) {
                    let temp = arr[i]
                    arr[i] = arr[i + 1]
                    arr[i + 1] = temp
                }
            }
        }
        return arr
    } else if(ascDesc === 'desc') {
        for (let j = len_arr - 1; j > 0; j--) {
            for (let i = 0; i < j; i++) {
                if (arr[i] < arr[i + 1]) {
                    let temp = arr[i]
                    arr[i] = arr[i + 1]
                    arr[i + 1] = temp
                }
            }
        }
        return arr
    }
}
console.log(sort(arr, 'desc'))

// Function Expression:
let sort = function sort(arr, ascDesc) {
    let len_arr = arr.length
    if (ascDesc === 'asc' || !ascDesc) {
        for (let j = len_arr - 1; j > 0; j--) {
            for (let i = 0; i < j; i++) {
                if (arr[i] > arr[i + 1]) {
                    let temp = arr[i]
                    arr[i] = arr[i + 1]
                    arr[i + 1] = temp
                }
            }
        }
        return arr
    } else if(ascDesc === 'desc') {
        for (let j = len_arr - 1; j > 0; j--) {
            for (let i = 0; i < j; i++) {
                if (arr[i] < arr[i + 1]) {
                    let temp = arr[i]
                    arr[i] = arr[i + 1]
                    arr[i + 1] = temp
                }
            }
        }
        return arr
    }
}
let asc = 'asc'
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
console.log(sort(arr))

// Function arrow:
let sort = (arr, ascDesc) => {
    let len_arr = arr.length
    if (ascDesc === 'asc' || !ascDesc) {
        for (let j = len_arr - 1; j > 0; j--) {
            for (let i = 0; i < j; i++) {
                if (arr[i] > arr[i + 1]) {
                    let temp = arr[i]
                    arr[i] = arr[i + 1]
                    arr[i + 1] = temp
                }
            }
        }
        return arr
    } else if(ascDesc === 'desc') {
        for (let j = len_arr - 1; j > 0; j--) {
            for (let i = 0; i < j; i++) {
                if (arr[i] < arr[i + 1]) {
                    let temp = arr[i]
                    arr[i] = arr[i + 1]
                    arr[i + 1] = temp
                }
            }
        }
        return arr
    }
}
let asc = 'asc'
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
console.log(sort(arr))

// Поиск в массиве:

// Function Declaration:
function arraySearch (arr, num) {
    let n = 0
    for (let i = 0; i < len_arr; i++) {
        if (num === arr[i]) {
            n++
        }
    }
    return `Количество встречающихся совпадений : ${n}`
}
let arr = [44, 12, 11, 7, 12, 44, 43, 5, 69]
let len_arr = arr.length
console.log(arraySearch(arr, 44))

// Function Expression:
 let arr_search = function arraySearch (arr, num) {
    let n = 0
    for (let i = 0; i < len_arr; i++) {
        if (num === arr[i]) {
            n++
        }
    }
    return `Количество встречающихся совпадений : ${n}`
}
let arr = [44, 12, 11, 7, 12, 44, 43, 5, 69]
let len_arr = arr.length
console.log(arr_search(arr, 44))

// Function arrow:
let arr_search = (arr, num) => {
    let n = 0
    for (let i = 0; i < len_arr; i++) {
        if (num === arr[i]) {
            n++
        }
    }
    return `Количество встречающихся совпадений : ${n}`
}
let arr = [44, 12, 11, 7, 12, 44, 43, 5, 69]
let len_arr = arr.length
console.log(arr_search(arr, 44))
