//1.
for:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let j = 0
for (let i = len_arr - 1; i >= 0; i--) {
    arr_1[j] = arr[i]
    j++
}
console.log(arr_1)

//while:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let i = len_arr - 1
let j = 0
while (i >= 0) {
    arr_1[j] = arr[i]
    i--
    j++
}
console.log(arr_1)

//do-while:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let i = len_arr - 1
let j = 0
do {
    arr_1[j] = arr[i]
    i--
    j++
}
while (i >= 0)
console.log(arr_1)

//for-of:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
for (let val of arr) {
    arr_1[len_arr] = val
    len_arr--
}
console.log(arr_1)

//for-in
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let i = 1
let j = 0
for (let val in arr) {
    arr_1[j] = arr[len_arr - i]
    i++
    j++
}
console.log(arr_1)

// 2.
// for:
let let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arr_1 = []
let len_arr = arr.length
let j = 0
for (let i = len_arr - 1; i >= 0; i--) {
    arr_1[j] = arr[i]
    j++
}
console.log(arr_1)

//while:
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arr_1 = []
let len_arr = arr.length
let i = len_arr - 1
let j = 0
while (i >= 0) {
    arr_1[j] = arr[i]
    i--
    j++
}
console.log(arr_1)

// do-while:
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arr_1 = []
let len_arr = arr.length
let i = len_arr - 1
let j = 0
do {
    arr_1[j] = arr[i]
    i--
    j++
}
while (i >= 0)
console.log(arr_1)

//for-of:
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arr_1 = []
let len_arr = arr.length
for (let val of arr) {
    arr_1[len_arr] = val
    len_arr--
}
console.log(arr_1)

//for-in :
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arr_1 = []
let i = 1
let j = 0
let len_arr = arr.length
for (let val in arr) {
    arr_1[j] = arr[len_arr - i]
    i++
    j++
}
console.log(arr_1)

//3.
//for:
let str = 'Hi I am ALex'
let str_1 = ''
for (let i = str.length - 1; i >= 0; i--) {
    str_1 += str[i]
}
console.log(str_1)

//while:
let str = 'Hi I am ALex'
let str_1 = ''
let i = str.length - 1
while (i >= 0) {
    str_1 += str[i]
    i--
}
console.log(str_1)

//do-while:
let str = 'Hi I am ALex'
let str_1 = ''
let i = str.length - 1
do {
    str_1 += str[i]
    i--
}
while (i >= 0)
console.log(str_1)

//for in:
let str = 'Hi I am ALex'
let str_1 = ''
for (let val in str) {
    str_1 = str[val] + str_1
}
console.log(str_1)

//for-of:
let str = 'Hi I am ALex'
let str_1 = ''
let len_str = str.length
for (let val of str) {
    str_1 = val + str_1
}
console.log(str_1)

//4.
let str = 'Hi I am ALex'
console.log(str.toLowerCase())

//5.
let str = 'Hi I am ALex'
console.log(str.toUpperCase())

//7.
// for:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let j = 1
let len_arr_1 = arr_1.length
for (let i = 0; i < len_arr; i++) {
    arr_1[len_arr_1] = arr[len_arr - j].toLowerCase()
    j++
    len_arr_1++
}
console.log(arr_1)

//while:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let i = 0
let j = 1
let len_arr_1 = arr_1.length
while (i < len_arr) {
    arr_1[len_arr_1] = arr[len_arr - j].toLowerCase()
    i++
    j++
    len_arr_1++
}
console.log(arr_1)

//do_while:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let i = 0
let j = 1
let len_arr_1 = arr_1.length
do {
    arr_1[len_arr_1] = arr[len_arr - j].toLowerCase()
    i++
    j++
    len_arr_1++
}
while (i < len_arr)
console.log(arr_1)

//for-of:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let res = []
let len_arr = arr.length
for (val of arr) {
    res[len_arr] = val.toLowerCase()
    len_arr--
}
console.log(res)

//for-in:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let res = []
let len_arr = arr.length
let i = 1
let j = 0
for (val in arr) {
    res[j] = arr[len_arr - i].toLowerCase()
    i++
    j++
}
console.log(res)

//8.
//for:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let len_arr = arr.length
let len_arr_1 = arr_1.length
let j = 1
for (let i = 0; i < len_arr; i++) {
    arr_1[len_arr_1] = arr[len_arr - j].toUpperCase()
    j++
    len_arr_1++
}
console.log(arr_1)

// while:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let i = 0
let len_arr = arr.length
let len_arr_1 = arr_1.length
let j = 1
while (i < len_arr) {
    arr_1[len_arr_1] = arr[len_arr - j].toUpperCase()
    i++
    j++
    len_arr_1++
}
console.log(arr_1)

// do_while:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arr_1 = []
let i = 0
let len_arr = arr.length
let len_arr_1 = arr_1.length
let j = 1
do {
    arr_1[len_arr_1] = arr[len_arr - j].toUpperCase()
    i++
    j++
    len_arr_1++
}
while (i < len_arr)
console.log(arr_1)

//for-of:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let res = []
let len_arr = arr.length
for (val of arr) {
    res[len_arr] = val.toUpperCase()
    len_arr--
}
console.log(res)
//
// for-in:
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let res = []
let len_arr = arr.length
let len_res = res.length
let i = 1
for (val in arr) {
    res[len_res] = arr[len_arr - i].toUpperCase()
    i++
    len_res++
}
console.log(res)

//9.
//for:
let num = 123
let str_num = String(num)
let str_1 = ''
for (let i = str_num.length - 1; i >= 0; i--) {
    str_1 += str_num[i]
    i--
}
let num_str = Number(str_1)
console.log(num_str)

//while:
let num = 123
let str_num = String(num)
let str_1 = ''
let i = str_num.length - 1
while (i >= 0) {
    str_1 += str_num[i]
    i--
}
let num_str = Number(str_1)
console.log(num_str)

//do-while:
let num = 123
let str_num = String(num)
let str_1 = ''
let i = str_num.length - 1
do {
    str_1 += str_num[i]
    i--
}
while (i >= 0)
let num_str = Number(str_1)
console.log(num_str)

//for in:
let num = 123
let str_num = String(num)
let str_1 = ''
for (let val in str_num) {
    str_1 = str_num[val] + str_1
}
let num_str = Number(str_1)
console.log(num_str)

//for-of:
let num = 123
let str_num = String(num)
let str_1 = ''
for (let val of str_num) {
    str_1 = val + str_1
}
let num_str = Number(str_1)
console.log(num_str)

// //10.

//for:
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let len_arr = arr.length
for (let j = len_arr - 1; j > 0; j--) {
    for (let i = 0; i < j; i++) {
        if (arr[i] < arr[i + 1]) {
            let temp = arr[i]
            arr[i] = arr[i + 1]
            arr[i + 1] = temp
        }
    }
}
console.log(arr)

//while:
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let len_arr = arr.length
let j = len_arr - 1
while (j > 0) {
    let i = 0
    while (i < j) {
        if (arr[i] < arr[i + 1]) {
            let temp = arr[i]
            arr[i] = arr[i + 1]
            arr[i + 1] = temp
        }
        i++
    }
    j--
}
console.log(arr)