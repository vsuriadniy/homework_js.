//1.
let a = 57
let b = 56

result = b > a ? 'b > a' : 'a > b'
console.log(result)

if (b > a) {
    console.log('b > a')
} else {
    console.log('a >= b')
}

let min = 5
let max = 15

console.log(Math.floor(Math.random() * (max - min) + min))

let array = [12, 1, 43, 6]
let len = array.length
let max = array[0]                // вычисляем максимальное число
for (let i = 0; i < len; i++) {
    if (max < array[i]) {
        max = array[i]
    }
}
console.log(max)

let array = [12, 1, 43, 6]
let len = array.length
let min = array[0]                // вычисляем минимальное число
for (let i = 0; i < len; i++) {
    if (min > array[i]) {
        min = array[i]
    }
}
console.log(min)

//2.
let surName = 'Suryiadniy'
let firstName = 'Vladislav'
let sub = firstName.substring(0,1)
let lastName = 'Oleksandrovich'
let sub_2 = lastName.substring(0, 1)
console.log(`${surName} ${sub}. ${sub_2}.`)

//3.
let a = -3
let b = 1
let c = 3
let d = 12
let res_1 = 0
let res_2 = 0

if (a > b) {
    res_1 = a
} else {
    res_1 = b
}
if (c > d) {
    res_2 = c
} else {
    res_2 = d
}
if (res_1 > res_2) {
    console.log(`Максимальное число - ${res_1}`)
} else {
    console.log(`Максимальное число - ${res_2}`)
}

if (a < b) {
    res_1 = a
} else {
    res_1 = b
}
if (c < d) {
    res_2 = c
} else {
    res_2 = d
}
if (res_1 < res_2) {
    console.log(`Минимальное число - ${res_1}`)
} else {
    console.log(`Минимальное число - ${res_2}`)
}

res_1 = a > b ? a : b
res_2 = c > d ? c : d
res_1 > res_2 ? console.log(`Максимальное число - ${res_1}`) : console.log(`Максимальное число - ${res_2}`)

res_1 = a < b ? a : b
res_2 = c < d ? c : d
res_1 < res_2 ? console.log(`Минимальное число - ${res_1}`) : console.log(`Минимальное число - ${res_2}`)

//4.
let ab = 10
let bc = 10
let ac = 10

if (ab === bc && ab === ac) {
    console.log('Это равносторонний треугольник')
} else {
    console.log('Это не равносторонний треугольник')
}

ab === bc && ab === ac ? console.log('Это равносторонний треугольник') : console.log('Это не равносторонний треугольник')

//5.
let ab = 10
let bc = 10
let cd = 10
let ad = 10

if (ab === bc && ab === cd && ab === ad) {
    console.log('Квадрат')
} else if (ab === cd && bc === ad){
    console.log("Прямоугольник")
} else {
    console.log("Неизвестная фигура !")
}

ab === bc && ab === cd && ab === ad ? console.log('Квадрат') :
    ab === cd && bc === ad ? console.log("Прямоугольник") : console.log("Неизвестная фигура !")

//6.
let num_1 = String( Math.floor( Math.random() * 10000) )  // num_1.length - так делать нельзя, почему?
let num_2 = '5'
let j = 0
let length = num_1.length
for ( let i = 0; i <= length; i++) {
    if ( num_2 === num_1.substr( i, num_2.length ) )
        j++
}
console.log(`${j} раз(а) встречается `)

// 7.
let month = 2;
if (month >= 1 && month <= 2 || month == 12) {
    console.log('winter')
} else if (month >= 3 && month <= 5) {
    console.log('spring')
} else if (month >= 6 && month <= 8) {
    console.log('summer')
} else if (month >= 9 && month <= 11) {
    console.log('othem')
} else {
    console.log('Не знаешь сколько месяцев в году !')
}

month >= 1 && month <= 2 || month == 12 ? console.log('winter') :
    month >= 3 && month <= 5 ? console.log('spring') :
        month >= 6 && month <= 8 ? console.log('summer') :
            month >= 9 && month <= 11 ? console.log('othem') : console.log('Не знаешь сколько месяцев в году !')

//8.
let str = 'abcde'
if (str[0] === 'a'){
    console.log('Da')
} else {
    console.log('Net')
}

str[0] === 'a' ? console.log('Da') : console.log('Net')

//9.
let str = '12345'
if (str[0] === '1' || str[1] === '2' || str[2] === '3'){
    console.log('Da')
} else {
    console.log('Net')
}

str[0] === '1' || str[1] === '2' || str[2] === '3' ? console.log('Da') : console.log('Net')

// 10.
let test = true
if (test == true) {
    console.log('Verno')
} else {
    console.log('Neverno')
}

test == true ? console.log('Verno') : console.log('Neverno')

//11.
let lang = 'en'
if (lang == 'ru') {
    console.log($arr = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'])
} else if (lang == 'en') {
    console.log(arr = ['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn'])
}

lang == 'ru' ? console.log(arr = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']) :
    lang == 'en' ? console.log(arr = ['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn']) : ''

//12.
let clock = 15
if (clock >= 1 && clock <= 15) {
    console.log('Первая четверть часа')
} else if (clock >= 16 && clock <= 30) {
    console.log('Вторая четверть часа')
} else if (clock >= 31 && clock <= 45) {
    console.log('Тертья четверть часа')
} else if (clock >= 46 && clock <= 60) {
    console.log('Четвертая четверть часа')
} else {
    console.log('В часе 60 минут !')
}

clock >= 1 && clock <= 15 ? console.log('Первая четверть часа') :
    clock >= 16 && clock <= 30 ? console.log('Вторая четверть часа') :
        clock >= 31 && clock <= 45 ? console.log('Тертья четверть часа') :
            clock >= 46 && clock <= 60 ? console.log('Четвертая четверть часа') : console.log('В часе 60 минут !')